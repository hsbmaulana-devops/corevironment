#!/bin/sh

# su - root #
# sh <(curl -sL https://gitlab.com/hsbmaulana-devops/corevironment/-/raw/master/.core.sh) #
#
export CHARSET='UTF-8'
export LANGUAGE='en_US.UTF-8'
me='hsbmaulana'
circle='hsbmaulana'
#
#
#
#
#
echo "Have you run 'curl -L ... -o ~/.gnupg/id.asc && gpg --import ~/.gnupg/id.asc' (Y/N) ? " && read var_gpg
echo "Have you run 'curl -L ... -o ~/.ssh/id_rsa && ssh-add ~/.ssh/id_rsa' (Y/N) ? " && read var_ssh
echo ''
#
#
#
if [ $var_gpg != 'Y' -o $var_ssh != 'Y' ]; then
    exit 1
else
    echo '...'
fi





# rootowner #

# sudo su - root #
echo "Do you want to setup the root password (Y/N) ? " && read var_root && if [ $var_root = 'Y' ]; then passwd root; fi
{ egrep '^deb' /etc/apt/sources.list; echo ''; grep -P -i '^(\#)\s(?=deb)' /etc/apt/sources.list; } | cat 1> /etc/apt/sources.listed && mv /etc/apt/sources.listed /etc/apt/sources.list
#
#
#
apt update
apt install -y snapd
#
#
#
apt install -y vagrant virtualbox && apt install -y docker.io docker-compose # docker.swarm #
apt install -y git
curl -sL https://cli-assets.heroku.com/install-ubuntu.sh | sh
#
snap install chromium # firefox #
snap install --classic code
snap install --edge keybase
#
apt install -y tree xclip expect
apt install -y android-tools-adb android-tools-fastboot kazam

printf '#!/bin/sh\n\neval $(ssh-agent)\nssh-add ~/.ssh/id_rsa' 1> /usr/local/sbin/sshing && chmod 755 /usr/local/sbin/sshing && chown $me:$circle /usr/local/sbin/sshing
#
#
#
groupadd --system -f vagrant && groupadd --system -f docker
usermod -aG vagrant,docker $me
#
rm -rf /etc/apt/sources.list.d/*.list
#
sed -i '/^#/d' /etc/nsswitch.conf
sed -i '/^#/d' /etc/resolv.conf
sed -i '/^#/d' /etc/hosts
#
sed -i '/^#/d' /etc/anacrontab
sed -i '/^#/d' /etc/crontab
#
su - $me -c "gsettings set com.ubuntu.update-notifier no-show-notifications true" 2> /dev/null
su - $me -c "gsettings set org.gnome.software download-updates false" 2> /dev/null
su - $me -c "gsettings set org.compiz.unityshell:/org/compiz/profiles/unity/plugins/unityshell/ launcher-minimize-window true" 2> /dev/null
su - $me -c "gsettings set org.gnome.nautilus.icon-view default-zoom-level 'small'" 2> /dev/null
su - $me -c "gsettings set com.canonical.Unity.Launcher launcher-position 'Bottom'" 2> /dev/null





# generalowner #

su - $me -c "sed -i '/^#\s/d' ~/.vimrc" 2> /dev/null
su - $me -c "sed -i '/^#\s/d' ~/.nanorc" 2> /dev/null
su - $me -c "sed -i '/^#\s/d' ~/.profile" 2> /dev/null
su - $me -c "sed -i '/^#\s/d' ~/.bash_logout" 2> /dev/null
su - $me -c "sed -i '/^#\s/d' ~/.bash_aliases" 2> /dev/null
# su - $me -c "sed -i -z 's/\n/,/g;s/,,,uname -a,cat <(echo),cd ~\/Desktop\/,cat <(echo),//g;s/,/\n/g' ~/.bashrc && echo -e '\n\n\nuname -a\ncat <(echo)\ncd ~/Desktop/\ncat <(echo)' 1>> ~/.bashrc" 2> /dev/null #





# configuration #

# su - root -c "sed -i -z 's/\n/,/g;s/,options snd-hda-intel position fix=1,options snd-hda-intel model=auto,//g;s/,/\n/g' /etc/modprobe.d/alsa-base.conf && echo -e '\noptions snd-hda-intel position fix=1\noptions snd-hda-intel model=auto' 1>> /etc/modprobe.d/alsa-base.conf" 2> /dev/null && pulseaudio --kill && pulseaudio --start #
#
#
#
#
#
su $me -c "echo -e '\nHost gitlab\n\tHostname gitlab.com\n\tPort 22\n\tUser git\n\tIdentityFile ~/.ssh/id_rsa\n\tIdentitiesOnly yes\n\tCompression yes\n\tAddKeysToAgent yes\n#ssh heroku #git clone ssh://git@gitlab.com/<owner>/<repository>.git -p 22\n\nHost github\n\tHostname github.com\n\tPort 22\n\tUser git\n\tIdentityFile ~/.ssh/id_rsa\n\tIdentitiesOnly yes\n\tCompression yes\n\tAddKeysToAgent yes\n#ssh gitlab #git clone ssh://git@github.com/<owner>/<repository>.git -p 22\n\nHost heroku\n\tHostname heroku.com\n\tPort 22\n\tUser git\n\tIdentityFile ~/.ssh/id_rsa\n\tIdentitiesOnly yes\n\tCompression yes\n\tAddKeysToAgent yes\n#ssh heroku #git clone ssh://git@heroku.com/<owner>/<repository>.git -p 22\n' 1> ~/.ssh/config && chmod 600 ~/.ssh/config"
#
#
#
#
#
su - $me -c "echo 'What is git email ? ' && read var_std && git config --global user.email $var_std && unset var_std" 2> /dev/null
su - $me -c "echo 'What is git name ? ' && read var_std && git config --global user.name $var_std && unset var_std" 2> /dev/null
su - $me -c "echo 'What is git signkey ? ' && read var_std && git config --global user.signingkey $var_std && unset var_std" 2> /dev/null
su - $me -c "git config --global core.editor 'code --wait'" 2> /dev/null
#
# su - $me -c 'heroku auth:login --interactive' 2> /dev/null #
#
#
#
su - $me -c "echo { 'editor.fontSize': 13, 'editor.fontFamily': \"'Droid Sans Mono', 'monospace', monospace, 'Droid Sans Fallback\", \'editor.insertSpaces\': true, \'editor.tabSize\': 2, \'editor.detectIndentation\': false } 1> ~/.config/Code/User/settings.json" 2> /dev/null
#
su - $me -c "echo -e 'set softtabstop=2\nset tabstop=8\nset shiftwidth=2\nset noswapfile\nset autoindent\nset background=dark' 1> ~/.vimrc" 2> /dev/null
su - $me -c "echo -e 'set tabsize 4\nset tabstospaces\nset morespace\nset nohelp\nset autoindent\nset statuscolor black\nset titlecolor white' 1> ~/.nanorc" 2> /dev/null
#
#
#
#
#
journalctl --flush --rotate && journalctl -m --vacuum-time=1s
rm -rf /var/crash/*

su - $me -c 'echo null 1> ~/.bash_history' && echo null 1> ~/.bash_history
date
