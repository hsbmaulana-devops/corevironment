# Fig pre block. Keep at the top of this file.
[[ -f "$HOME/.fig/shell/zshrc.pre.zsh" ]] && builtin source "$HOME/.fig/shell/zshrc.pre.zsh"

GPG_TTY=$(tty)

export HOMEBREW_NO_AUTO_UPDATE=1

export PATH="$PATH:$HOME/.rvm/bin"
#export JAVA_HOME=/usr/libexec/java_home

export ANDROID_HOME=$HOME/Library/Android/sdk
#export XCODE_HOME=

export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools



trash() { mv -f -v $@ ~/.Trash/`date +%Y%m%d%H%M%S` ; }



alias l="ls -A -l -h"
alias rm="trash"
alias rmdir="trash"



# PHP & Node.js #

export PATH="/usr/local/opt/php@8.0/bin:$PATH"
export PATH="/usr/local/opt/php@8.0/sbin:$PATH"

export PHPENV_ROOT="${HOME}/.phpenv"
if [[ -d "${PHPENV_ROOT}" ]]; then
    export PATH="${PHPENV_ROOT}/bin:${PATH}";
    eval "$(phpenv init -)";
fi

export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh"
[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && \. "/usr/local/opt/nvm/etc/bash_completion"

# Python & Ruby #

alias python="python3"
#alias ruby=

#alias pyenv=
#alias rvm=

#alias pip="python -m pip3"
alias pipenv="python -m pipenv"
#alias venv="python -m venv"
alias virtualenv="python -m virtualenv"
#alias bundler=

# Fig post block. Keep at the bottom of this file.
[[ -f "$HOME/.fig/shell/zshrc.post.zsh" ]] && builtin source "$HOME/.fig/shell/zshrc.post.zsh"
