# Fig pre block. Keep at the top of this file.
[[ -f "$HOME/.fig/shell/bashrc.pre.bash" ]] && builtin source "$HOME/.fig/shell/bashrc.pre.bash"

export HOMEBREW_NO_AUTO_UPDATE=1

export PATH="$PATH:$HOME/.rvm/bin"
#export JAVA_HOME=/usr/libexec/java_home

export ANDROID_HOME=$HOME/Library/Android/sdk
#export XCODE_HOME=

export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools



trash() { mv -f -v $@ ~/.Trash/`date +%Y%m%d%H%M%S` ; }



alias rm="trash"
alias rmdir="trash"



# Fig post block. Keep at the bottom of this file.
[[ -f "$HOME/.fig/shell/bashrc.post.bash" ]] && builtin source "$HOME/.fig/shell/bashrc.post.bash"
