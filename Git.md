<center>
<table>
<tbody>

<tr>
<td align="center" width="9999">git config --system --list</td>
<td align="center" width="9999">git config --global --list</td>
<td align="center" width="9999">git config --local --list</td>
</tr>
<tr>
<td align="center" width="9999">git config --system core.x "..."</td>
<td align="center" width="9999">git config --global core.y "..."</td>
<td align="center" width="9999">git config --local core.z "..."</td>
</tr>
<tr>
<td align="center" width="9999">git config --system --unset core.x</td>
<td align="center" width="9999">git config --global --unset core.y</td>
<td align="center" width="9999">git config --local --unset core.z</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">protocol://<br/>.../<br/>owner/<br/>repository/<br/>invitations</td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">git init</td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">git clone protocol://...<br/><br/>git clone --single-branch -b ... protocol://...<br/><br/>git clone --depth 1 -b vX.Y.Z protocol://...</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">git add ...</td>
<td align="center" width="9999">git status</td>
<td align="center" width="9999">git rm -r ...</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">git stash push -m "..."</td>
<td align="center" width="9999">git stash list<br/>git stash show stash@{...}</td>
<td align="center" width="9999">git stash pop stash@{...}</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">git commit -m "..."</td>
<td align="center" width="9999">git log</td>
<td align="center" width="9999">git revert <i>hash</i> -m "..."<br/>git reset --hard|--soft|--mixed <i>hash</i></td>
</tr>
<tr>
<td align="center" width="9999">git tag vX.Y.Z <i>hash</i> -m "..."</td>
<td align="center" width="9999">git tag</td>
<td align="center" width="9999">git tag -d ... && git push -d <i>remote</i> ...</td>
</tr>
<tr>
<td align="center" width="9999">git branch ... && git checkout ...</td>
<td align="center" width="9999">git branch -a</td>
<td align="center" width="9999">git branch -d ... && git push -d <i>remote</i> ...</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">git remote -v</i></td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">git push <i>remote</i> <i>branch</i></td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">git push <i>remote</i> --tags</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">git pull <i>remote</i> <i>branch</i></td>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">git fetch <i>remote</i> <i>branch</i><br/><br/>git merge --squash <i>remote</i>/<i>branch</i><br/><br/>git checkout feature && git rebase master<br/>&&<br/>git checkout master && git rebase feature</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">git diff <i>old</i>..<i>remote</i>/<i>new</i></td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">git log <i>old</i>..<i>remote</i>/<i>new</i></td>
<td align="center" width="9999">&nbsp;</td>
</tr>

</tbody>
</table>
</center>
