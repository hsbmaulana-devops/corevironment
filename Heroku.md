<center>
<table>
<tbody>

<tr>
<td align="center" width="9999">heroku auth:login<br/>--interactive|--browser</td>
<td align="center" width="9999">heroku auth:whoami</td>
<td align="center" width="9999">heroku auth:logout</td>
</tr>
<tr>
<td align="center" width="9999">heroku keys:add /home/key.pub</td>
<td align="center" width="9999">heroku keys</td>
<td align="center" width="9999">heroku keys:remove owner@email.com</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">heroku apps:create ...<br/>--stack ...<br/>--buildpack ...<br/>--addons ...,...,...</td>
<td align="center" width="9999">heroku apps</td>
<td align="center" width="9999">heroku apps:destroy -a ...</td>
</tr>
<tr>
<td align="center" width="9999">heroku config:set ...=... -a ...</td>
<td align="center" width="9999">heroku config -a ...</td>
<td align="center" width="9999">heroku config:unset ... -a ...</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">heroku ps:restart ... -a ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">heroku ps:exec '...' -a ... -d ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">heroku run bash -a ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">heroku plugins:install ...</td>
<td align="center" width="9999">heroku plugins --core</td>
<td align="center" width="9999">heroku plugins:uninstall ...</td>
</tr>
<tr>
<td align="center" width="9999">heroku buildpacks:add ... -a ...</td>
<td align="center" width="9999">heroku buildpacks -a ...<br/>heroku buildpacks:search ...</td>
<td align="center" width="9999">heroku buildpacks:remove ... -a ...</td>
</tr>
<tr>
<td align="center" width="9999">heroku addons:create ...<br/>&&<br/>heroku addons:attach ... -a ...</td>
<td align="center" width="9999">heroku addons -a ...<br/>heroku addons:services</td>
<td align="center" width="9999">heroku addons:detach ... -a ...<br/>&&<br/>heroku addons:destroy ...</td>
</tr>

<tr><td align="center" width="9999" colspan="3">&nbsp;</td></tr>

<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">heroku ps -a ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">heroku releases -a ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>
<tr>
<td align="center" width="9999">&nbsp;</td>
<td align="center" width="9999">heroku logs -a ...</td>
<td align="center" width="9999">&nbsp;</td>
</tr>

</tbody>
</table>
</center>
